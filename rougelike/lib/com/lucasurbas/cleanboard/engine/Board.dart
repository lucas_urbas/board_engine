import 'dart:collection';
import 'dart:math';
import 'dart:ui';
import 'package:flame/components.dart';
import 'package:flutter/material.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/Tile.dart';

class Board extends PositionComponent {
  final Paint _paint = Paint();
  final Paint _paintSecond = Paint();
  Rect _boardRect = Rect.fromLTWH(0, 0, 0, 0);

  final int boardWidth;
  final int boardHeight;

  List<List<Tile>> tileMap;
  Map<int, List<Tile>> _layers = HashMap<int, List<Tile>>();

  double _cameraTileSize = 0;
  double _zoom = 0;
  Vector2 _cameraCanvasPosition;
  Vector2 _cameraCenterPosition;

  Board(this.boardWidth, this.boardHeight) {
    _paint.color = Color.fromARGB(255, 255, 0, 0);
    _paint.style = PaintingStyle.stroke;
    _paint.strokeWidth = 2;

    _paintSecond.color = Color.fromARGB(50, 255, 0, 0);
    _paintSecond.style = PaintingStyle.stroke;
    _paintSecond.strokeWidth = 1;

    _initBoard();
  }

  void _initBoard() {
    tileMap =
        List.generate(boardWidth, (i) => List(boardHeight), growable: false);

    for (var x = 0; x < boardWidth; x++) {
      for (var y = 0; y < boardHeight; y++) {
        tileMap[x][y] = Tile(this, x, y);
      }
    }
  }

  @override
  void onGameResize(Vector2 gameSize) {
    super.onGameResize(gameSize);
    size = gameSize;
  }

  @override
  void render(Canvas c) {
    if (_cameraCanvasPosition == null || _cameraCenterPosition == null) {
      return;
    }
    c.clipRect(Rect.fromLTWH(0, 0, size.x, size.y));
    c.save();
    c.translate(_cameraCanvasPosition.x, _cameraCanvasPosition.y);

    // for (var x = 0; x < boardWidth; x++) {
    //   for (var y = 0; y < boardHeight; y++) {
    //     c.drawRect(
    //         Rect.fromLTWH(_cameraTileSize * x, _cameraTileSize * y,
    //             _cameraTileSize, _cameraTileSize),
    //         _paintSecond);
    //   }
    // }
    // c.drawRect(_boardRect, _paint);

    for (var i = 0; i < _layers.keys.length; i++) {
      List tilesAtLayer = _layers[i];
      for (Tile tile in tilesAtLayer) {
        // c.save();
        Vector2 tileCenter = Vector2(
            _cameraCanvasPosition.x +
                _cameraTileSize * tile.x +
                _cameraTileSize / 2,
            _cameraCanvasPosition.y +
                _cameraTileSize * tile.y +
                _cameraTileSize / 2);

        double xFactor = i *
            (tileCenter.x - _cameraCenterPosition.x) /
            (10 + (50 * (_zoom)));
        double yFactor = i *
            (tileCenter.y - _cameraCenterPosition.y) /
            (5 + (30 * (_zoom)));

        double layerOffsetX = xFactor;
        double layerOffsetY = yFactor;

        double dx = _cameraTileSize * tile.x + layerOffsetX;
        double dy = _cameraTileSize * tile.y + layerOffsetY;
        tile.getPaintLayer(i).render(dx, dy, c);
        // c.restore();
      }
    }
    c.restore();
  }

  @override
  void update(double t) {}

  void updateCamera(double tileSize, double zoom, Vector2 cameraCanvasPosition,
      Vector2 cameraCenterPosition) {
    _cameraCanvasPosition = cameraCanvasPosition;
    _cameraCenterPosition = cameraCenterPosition;

    if (_cameraTileSize != tileSize && tileSize != null) {
      _cameraTileSize = tileSize;
      _zoom = zoom;

      _boardRect = Rect.fromLTWH(
          0, 0, _cameraTileSize * boardWidth, _cameraTileSize * boardHeight);

      for (var i = 0; i < _layers.keys.length; i++) {
        List tilesAtLayer = _layers[i];
        double layerAlpha = _layerAlpha(i, _layers.keys.length);
        for (Tile tile in tilesAtLayer) {
          tile.getPaintLayer(i).updateSize(_cameraTileSize);
          // tile.getPaintLayer(i).updateAlpha(layerAlpha);
        }
      }
    }
  }

  double _layerAlpha(int layer, int topLayer) {
    return (layer * 0.6) / topLayer + 0.4;
  }

  void removeFromHeight(int height, Tile tile) {
    for (var x = 0; x < height; x++) {
      List tilesAtLayer = _layers[x];
      if (tilesAtLayer != null && tilesAtLayer.contains(tile)) {
        tilesAtLayer.remove(tile);
        if (tilesAtLayer.isEmpty) {
          _layers.remove(x);
        }
      }
    }
  }

  void addToHeight(int height, Tile tile) {
    for (var x = 0; x < height; x++) {
      List tilesAtLayer = _layers[x];
      if (tilesAtLayer == null) {
        tilesAtLayer = List<Tile>();
        _layers[x] = tilesAtLayer;
      }
      tilesAtLayer.add(tile);
    }
  }
}
