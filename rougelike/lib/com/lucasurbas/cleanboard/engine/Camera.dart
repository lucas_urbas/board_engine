import 'dart:ui';
import 'package:flame/components.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/Board.dart';
import 'dart:math';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/ValueAnimation.dart';

class GameCamera extends PositionComponent {
  static const double ANIM_DURATION = 0.3;

  final int _boardWidthTile;
  final int _boardHeightTile;
  double _primarySize;
  double _secondarySize;
  final Board _board;

  final Paint _paint = Paint();
  double _padding = 12.0;

  Vector2 _position = Vector2.zero();
  Vector2 _positionTile = Vector2.zero();
  Vector2 _offset = Vector2.zero();
  double tileSize;

  Rect _cameraRect = Rect.fromLTWH(0, 0, 0, 0);

  double _widthTile;
  double _heightTile;

  ValueAnimation _animPrimarySize = ValueAnimation(0, 0, 0);
  ValueAnimation _animSecondarySize = ValueAnimation(0, 0, 0);

  ValueAnimation _animPosX = ValueAnimation(0, 0, 0);
  ValueAnimation _animPosY = ValueAnimation(0, 0, 0);

  TextComponent _tilePositionText = TextComponent(text: "0,0");

  GameCamera(this._boardWidthTile, this._boardHeightTile, this._primarySize,
      this._board) {
    _paint.color = Color.fromARGB(255, 0, 0, 0);
    _paint.style = PaintingStyle.stroke;
    _paint.strokeWidth = 1;
  }

  @override
  void onGameResize(Vector2 gameSize) {
    super.onGameResize(gameSize);
    size = gameSize;
    _updateSize(true);
  }

  @override
  void render(Canvas c) {
    c.drawRect(_cameraRect, _paint);
    c.drawCircle(_cameraRect.center, 2, _paint);

    _tilePositionText.render(c);
  }

  @override
  void update(double t) {
    _animPrimarySize.update(t);
    _animSecondarySize.update(t);
    _animPosX.update(t);
    _animPosY.update(t);

    if (_animPrimarySize.isRunning()) {
      _setPrimarySize(_animPrimarySize.current);
      // _updateSize(false);
    }
    if (_animSecondarySize.isRunning()) {
      _setSecondarySize(_animSecondarySize.current);
      _updateSize(false);
    }

    if (_animPosX.isRunning()) {
      double oldY = _position.y;
      _position = Vector2(_animPosX.current, oldY);
    }
    if (_animPosY.isRunning()) {
      double oldX = _position.x;
      _position = Vector2(oldX, _animPosY.current);
    }

    _board.updateCamera(tileSize, (_primarySize - 4) / 17, _getCanvasPosition(),
        _getCenterPosition());
  }

  Vector2 _getCanvasPosition() {
    return _offset - _position;
  }

  Vector2 _getCenterPosition() {
    return Vector2(_cameraRect.center.dx, _cameraRect.center.dy);
  }

  void zoomIn() {
    double newPrimarySize = (_primarySize - 2).round().toDouble();
    if (newPrimarySize % 2 == 0) {
      newPrimarySize = newPrimarySize - 1;
    }
    double newSecondarySize =
        _getNewSecondarySize(_getTileSize(newPrimarySize));

    _tilePositionText.text = "$newSecondarySize";

    if (newPrimarySize >= 4) {
      _animPrimarySize = ValueAnimation(
          ANIM_DURATION, _primarySize, newPrimarySize, callback: () {
        _setPrimarySize(newPrimarySize);
        _updateSize(false);
      })
        ..start();

      _animSecondarySize = ValueAnimation(
          ANIM_DURATION, _secondarySize, newSecondarySize, callback: () {
        _setSecondarySize(newSecondarySize);
        _updateSize(false);
      })
        ..start();
    }
  }

  void zoomOut() {
    double newPrimarySize = (_primarySize + 2).round().toDouble();
    if (newPrimarySize % 2 == 0) {
      newPrimarySize = newPrimarySize - 1;
    }
    double newSecondarySize =
        _getNewSecondarySize(_getTileSize(newPrimarySize));

    _tilePositionText.text = "$newSecondarySize";

    if (newPrimarySize <= 21) {
      _animPrimarySize = ValueAnimation(
          ANIM_DURATION, _primarySize, newPrimarySize, callback: () {
        _setPrimarySize(newPrimarySize);
        _updateSize(false);
      })
        ..start();

      _animSecondarySize = ValueAnimation(
          ANIM_DURATION, _secondarySize, newSecondarySize, callback: () {
        _setSecondarySize(newSecondarySize);
        _updateSize(false);
      })
        ..start();
    }
  }

  void _setPrimarySize(double newSize) {
    _primarySize = newSize;
    if (size.x < size.y) {
      _widthTile = newSize;
      tileSize = (size.x - (2 * _padding)) / _widthTile;
    } else {
      _heightTile = newSize;
      tileSize = (size.y - (2 * _padding)) / _heightTile;
    }
  }

  double _getTileSize(double newPrimarySize) {
    if (size.x < size.y) {
      return (size.x - (2 * _padding)) / newPrimarySize;
    } else {
      return (size.y - (2 * _padding)) / newPrimarySize;
    }
  }

  double _getNewSecondarySize(double newTileSize) {
    double newSize = 0;
    if (size.x < size.y) {
      newSize = ((size.y - (2 * _padding)) ~/ newTileSize).toDouble();
    } else {
      newSize = ((size.x - (2 * _padding)) ~/ newTileSize).toDouble();
    }
    if (newSize % 2 == 0) {
      newSize = newSize - 1;
    }
    return newSize;
  }

  void _setSecondarySize(double newSize) {
    _secondarySize = newSize;
    if (size.x < size.y) {
      _heightTile = newSize;
    } else {
      _widthTile = newSize;
    }
  }

  void _updateSize(bool recalculate) {
    if (_widthTile == null || _heightTile == null || recalculate) {
      if (size.x < size.y) {
        _widthTile = _primarySize;
        tileSize = (size.x - (2 * _padding)) / _widthTile;
        _secondarySize = _getNewSecondarySize(tileSize);
        _heightTile = _secondarySize;
      } else {
        _heightTile = _primarySize;
        tileSize = (size.y - (2 * _padding)) / _heightTile;
        _secondarySize = _getNewSecondarySize(tileSize);
        _widthTile = _secondarySize;
      }
    }

    double width = tileSize * _widthTile;
    double left = (size.x - width) / 2;
    double height = tileSize * _heightTile;
    double top = (size.y - height) / 2;
    _cameraRect = Rect.fromLTWH(left, top, width, height);
    _offset = Vector2(left, top);

    _tilePositionText.x = _offset.x;
    _tilePositionText.y = _offset.y;

    double newXPos = _calculateNewXPosition(_positionTile.x);
    double xTile = _calculateNewXTile(_positionTile.x);
    double newYPos = _calculateNewYPosition(_positionTile.y);
    double yTile = _calculateNewYTile(_positionTile.y);

    _position = Vector2(newXPos, newYPos);
    _positionTile = Vector2(xTile, yTile);
    // _tilePositionText.text = "$_primarySize,$_secondarySize";
  }

  void addPosition(Vector2 delta) {
    _animPosX.stop();
    _animPosY.stop();
    _position.add(delta);
  }

  void snap() {
    double newXTile = ((_position.x + tileSize / 2) ~/ tileSize).toDouble();
    double newYTile = ((_position.y + tileSize / 2) ~/ tileSize).toDouble();

    double newXPos = _calculateNewXPosition(newXTile);
    int xTile = _calculateNewXTile(newXTile).round();
    double newYPos = _calculateNewYPosition(newYTile);
    int yTile = _calculateNewYTile(newYTile).round();

    _positionTile = Vector2(xTile.toDouble(), yTile.toDouble());
    _tilePositionText.text = "$xTile,$yTile";

    _animPosX =
        ValueAnimation(ANIM_DURATION, _position.x, newXPos, callback: () {
      _position = Vector2(newXPos, newYPos);
    })
          ..start();

    _animPosY =
        ValueAnimation(ANIM_DURATION, _position.y, newYPos, callback: () {
      _position = Vector2(newXPos, newYPos);
    })
          ..start();
  }

  double _calculateNewXPosition(double valueToCheck) {
    double newXPosition;
    if (_widthTile > _boardWidthTile) {
      newXPosition = (_boardWidthTile - _widthTile) / 2;
    } else {
      newXPosition = max(valueToCheck, 0);
      newXPosition = min(newXPosition, _boardWidthTile - _widthTile);
    }
    return newXPosition * tileSize;
  }

  double _calculateNewYPosition(double valueToCheck) {
    double newYPosition;
    if (_heightTile > _boardHeightTile) {
      newYPosition = (_boardHeightTile - _heightTile) / 2;
    } else {
      newYPosition = max(valueToCheck, 0);
      newYPosition = min(newYPosition, _boardHeightTile - _heightTile);
    }
    return newYPosition * tileSize;
  }

  double _calculateNewXTile(double valueToCheck) {
    double newXTile;
    if (_widthTile > _boardWidthTile) {
      newXTile = 0;
    } else {
      double temp = max(valueToCheck, 0);
      temp = min(temp, _boardWidthTile - _widthTile);
      newXTile = temp;
    }
    return newXTile;
  }

  double _calculateNewYTile(double valueToCheck) {
    double newYTile;
    if (_heightTile > _boardHeightTile) {
      newYTile = 0;
    } else {
      double temp = max(valueToCheck, 0);
      temp = min(temp, _boardHeightTile - _heightTile);
      newYTile = temp;
    }
    return newYTile;
  }
}
