import 'dart:ui';
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/Board.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/Camera.dart';

abstract class CleanBoardGame extends FlameGame with MultiTouchDragDetector {
  final int boardWidth;
  final int boardHeight;
  final double initialCameraSize;
  final Color color;
  final Function(Board board) initBoard;

  Board board;
  GameCamera boardCamera;

  CleanBoardGame(
      {this.boardWidth = 20,
      this.boardHeight = 20,
      this.initialCameraSize = 10,
      this.color,
      this.initBoard}) {
    board = Board(boardWidth, boardHeight);
    boardCamera = GameCamera(boardWidth, boardHeight, initialCameraSize, board);
  }

  @override
  Future<void> onLoad() async {
    await super.onLoad();

    add(Background(color));
    add(board);
    add(boardCamera);

    if (initBoard != null) {
      initBoard(board);
    }
  }

  @override
  void onDragUpdate(int pointerId, DragUpdateInfo info) {
    Offset delta = info.raw.delta;
    Vector2 v = Vector2(delta.dx, delta.dy);
    boardCamera.addPosition(v.scaled(-1));
  }

  @override
  void onDragEnd(int pointerId, DragEndInfo info) {
    boardCamera.snap();
  }

// @override
// void onReceiveDrag(DragEvent event) {
//   onPanStart(event.initialPosition);
//
//   event
//     ..onUpdate = onPanUpdate
//     ..onEnd = onPanEnd
//     ..onCancel = onPanCancel;
// }
//
// void onPanCancel() {}
//
// void onPanStart(Offset position) {}
//
// void onPanUpdate(details) {
//   Offset delta = details.delta;
//   Position p = Position.fromOffset(delta);
//   boardCamera.addPosition(p.times(-1));
// }
//
// void onPanEnd(details) {
//   boardCamera.snap();
// }
}

class Background extends PositionComponent {
  final Paint bgPaint = Paint()..style = PaintingStyle.fill;

  Background(Color color) {
    bgPaint.color = color;
    bgPaint.style = PaintingStyle.fill;
  }

  @override
  void onGameResize(Vector2 gameSize) {
    super.onGameResize(gameSize);
    size = gameSize;
  }

  @override
  void render(Canvas c) {
    Rect bgRect = Rect.fromLTWH(0, 0, size.x, size.y);
    c.drawRect(bgRect, bgPaint);
  }

  @override
  void update(double t) {}
}
