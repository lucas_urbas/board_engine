import 'dart:math';

import 'package:rougelike/com/lucasurbas/cleanboard/engine/Board.dart';
import 'dart:ui';

class Tile {
  final Board _board;

  final List<Stackable> _objects = List();
  final List<PaintLayer> _paintLayers = List();

  final int x;
  final int y;

  Tile(this._board, this.x, this.y);

  void addObject(Stackable object) {
    _board.removeFromHeight(_getHeight(), this);
    _objects.add(object);
    _board.addToHeight(_getHeight(), this);

    _calculatePaintLayers();
  }

  int _getHeight() {
    int height = 0;
    for (Stackable s in _objects) {
      height += s.layers.length;
    }
    return height;
  }

  void _calculatePaintLayers() {
    _paintLayers.clear();
    for (Stackable s in _objects) {
      for (PaintLayer l in s.layers) {
        _paintLayers.add(l);
      }
    }
  }

  PaintLayer getPaintLayer(int position) {
    return _paintLayers[position];
  }
}

class Stackable {
  List<PaintLayer> layers = List();
}

class PaintLayer {
  void render(double dx, double dy, Canvas c) {}

  void updateSize(double size) {}

  // void updateAlpha(double alpha) {}
}
