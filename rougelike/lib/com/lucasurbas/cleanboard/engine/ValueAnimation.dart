
class ValueAnimation {
  final double _durationTime;
  double _currentTime = 0;

  final double _start;
  final double _end;
  void Function() _callback;
  double _current = 0;
  bool _running = false;

  ValueAnimation(this._durationTime, this._start, this._end, {void Function() callback}) {
    _callback = callback;
  }

  double get current => _current;

  void update(double dt) {
    if (_running) {
      _currentTime += dt;

      _current = _start + (_end - _start) * progress;

      if (isFinished()) {
        _running = false;

        if (_callback != null) {
          _callback();
        }
      }
    }
  }

  double get progress => _currentTime / _durationTime;

  bool isFinished() {
    return _currentTime >= _durationTime;
  }

  bool isRunning() => _running;

  void start() {
    _current = _start;
    _currentTime = 0;
    _running = true;
  }

  void stop() {
    _current = -1;
    _currentTime = 0;
    _running = false;
  }
}
