import 'dart:ui';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/CleanBoardGame.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/objects/objects.dart';

class Chess extends CleanBoardGame {
  Chess()
      : super(
            boardWidth: 8,
            boardHeight: 8,
            initialCameraSize: 8,
            color: Color(0xff7d7575),
            initBoard: (board) {
              for (var x = 0; x < 8; x++) {
                for (var y = 0; y < 8; y++) {
                  var pieceColor;
                  if ((x + y) % 2 == 0) {
                    pieceColor = PieceColor.WHITE;
                  } else {
                    pieceColor = PieceColor.BLACK;
                  }
                  board.tileMap[x][y].addObject(ChessFloor(pieceColor));
                }
              }

              board.tileMap[0][1].addObject(Pawn(PieceColor.BLACK));
              board.tileMap[1][1].addObject(Pawn(PieceColor.BLACK));
              board.tileMap[2][1].addObject(Pawn(PieceColor.BLACK));
              board.tileMap[3][1].addObject(Pawn(PieceColor.BLACK));
              board.tileMap[4][1].addObject(Pawn(PieceColor.BLACK));
              board.tileMap[5][1].addObject(Pawn(PieceColor.BLACK));
              board.tileMap[6][1].addObject(Pawn(PieceColor.BLACK));
              board.tileMap[7][1].addObject(Pawn(PieceColor.BLACK));

              board.tileMap[0][0].addObject(Rook(PieceColor.BLACK));
              board.tileMap[1][0].addObject(Rook(PieceColor.BLACK));
              board.tileMap[2][0].addObject(Rook(PieceColor.BLACK));
              board.tileMap[3][0].addObject(Rook(PieceColor.BLACK));
              board.tileMap[4][0].addObject(Rook(PieceColor.BLACK));
              board.tileMap[5][0].addObject(Rook(PieceColor.BLACK));
              board.tileMap[6][0].addObject(Rook(PieceColor.BLACK));
              board.tileMap[7][0].addObject(Rook(PieceColor.BLACK));

              board.tileMap[0][6].addObject(Pawn(PieceColor.WHITE));
              board.tileMap[1][6].addObject(Pawn(PieceColor.WHITE));
              board.tileMap[2][6].addObject(Pawn(PieceColor.WHITE));
              board.tileMap[3][6].addObject(Pawn(PieceColor.WHITE));
              board.tileMap[4][6].addObject(Pawn(PieceColor.WHITE));
              board.tileMap[5][6].addObject(Pawn(PieceColor.WHITE));
              board.tileMap[6][6].addObject(Pawn(PieceColor.WHITE));
              board.tileMap[7][6].addObject(Pawn(PieceColor.WHITE));

              board.tileMap[0][7].addObject(Rook(PieceColor.WHITE));
              board.tileMap[1][7].addObject(Rook(PieceColor.WHITE));
              board.tileMap[2][7].addObject(Rook(PieceColor.WHITE));
              board.tileMap[3][7].addObject(Rook(PieceColor.WHITE));
              board.tileMap[4][7].addObject(Rook(PieceColor.WHITE));
              board.tileMap[5][7].addObject(Rook(PieceColor.WHITE));
              board.tileMap[6][7].addObject(Rook(PieceColor.WHITE));
              board.tileMap[7][7].addObject(Rook(PieceColor.WHITE));
            });
}
