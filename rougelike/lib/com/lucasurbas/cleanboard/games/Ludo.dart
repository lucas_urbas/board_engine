import 'dart:ui';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/CleanBoardGame.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/objects/objects.dart';

class Ludo extends CleanBoardGame {
  Ludo()
      : super(
      boardWidth: 11,
      boardHeight: 11,
      initialCameraSize: 11,
      color: Color(0xffD0EAF4),
      initBoard: (board) {

        board.tileMap[4][0].addObject(LudoFloor(null));
        board.tileMap[5][0].addObject(LudoFloor(null));
        board.tileMap[6][0].addObject(LudoFloor(null));

        board.tileMap[4][1].addObject(LudoFloor(null));
        board.tileMap[6][1].addObject(LudoFloor(null));

        board.tileMap[4][2].addObject(LudoFloor(null));
        board.tileMap[6][2].addObject(LudoFloor(null));

        board.tileMap[4][3].addObject(LudoFloor(null));
        board.tileMap[6][3].addObject(LudoFloor(null));

        board.tileMap[0][4].addObject(LudoFloor(null));
        board.tileMap[1][4].addObject(LudoFloor(null));
        board.tileMap[2][4].addObject(LudoFloor(null));
        board.tileMap[3][4].addObject(LudoFloor(null));
        board.tileMap[4][4].addObject(LudoFloor(null));
        board.tileMap[6][4].addObject(LudoFloor(null));
        board.tileMap[7][4].addObject(LudoFloor(null));
        board.tileMap[8][4].addObject(LudoFloor(null));
        board.tileMap[9][4].addObject(LudoFloor(null));
        board.tileMap[10][4].addObject(LudoFloor(null));

        board.tileMap[0][5].addObject(LudoFloor(null));
        board.tileMap[10][5].addObject(LudoFloor(null));

        board.tileMap[0][6].addObject(LudoFloor(null));
        board.tileMap[1][6].addObject(LudoFloor(null));
        board.tileMap[2][6].addObject(LudoFloor(null));
        board.tileMap[3][6].addObject(LudoFloor(null));
        board.tileMap[4][6].addObject(LudoFloor(null));
        board.tileMap[6][6].addObject(LudoFloor(null));
        board.tileMap[7][6].addObject(LudoFloor(null));
        board.tileMap[8][6].addObject(LudoFloor(null));
        board.tileMap[9][6].addObject(LudoFloor(null));
        board.tileMap[10][6].addObject(LudoFloor(null));

        board.tileMap[4][7].addObject(LudoFloor(null));
        board.tileMap[6][7].addObject(LudoFloor(null));

        board.tileMap[4][8].addObject(LudoFloor(null));
        board.tileMap[6][8].addObject(LudoFloor(null));

        board.tileMap[4][9].addObject(LudoFloor(null));
        board.tileMap[6][9].addObject(LudoFloor(null));

        board.tileMap[4][10].addObject(LudoFloor(null));
        board.tileMap[5][10].addObject(LudoFloor(null));
        board.tileMap[6][10].addObject(LudoFloor(null));

        board.tileMap[0][0].addObject(LudoFloor(LudoColor.RED));
        board.tileMap[1][0].addObject(LudoFloor(LudoColor.RED));
        board.tileMap[0][1].addObject(LudoFloor(LudoColor.RED));
        board.tileMap[1][1].addObject(LudoFloor(LudoColor.RED));

        board.tileMap[1][5].addObject(LudoFloor(LudoColor.RED));
        board.tileMap[2][5].addObject(LudoFloor(LudoColor.RED));
        board.tileMap[3][5].addObject(LudoFloor(LudoColor.RED));
        board.tileMap[4][5].addObject(LudoFloor(LudoColor.RED));

        board.tileMap[0][1].addObject(LudoPawn(LudoColor.RED));
        board.tileMap[1][1].addObject(LudoPawn(LudoColor.RED));
        board.tileMap[1][0].addObject(LudoPawn(LudoColor.RED));
        board.tileMap[0][0].addObject(LudoPawn(LudoColor.RED));

        board.tileMap[9][0].addObject(LudoFloor(LudoColor.BLUE));
        board.tileMap[10][0].addObject(LudoFloor(LudoColor.BLUE));
        board.tileMap[9][1].addObject(LudoFloor(LudoColor.BLUE));
        board.tileMap[10][1].addObject(LudoFloor(LudoColor.BLUE));

        board.tileMap[5][1].addObject(LudoFloor(LudoColor.BLUE));
        board.tileMap[5][2].addObject(LudoFloor(LudoColor.BLUE));
        board.tileMap[5][3].addObject(LudoFloor(LudoColor.BLUE));
        board.tileMap[5][4].addObject(LudoFloor(LudoColor.BLUE));

        board.tileMap[7][4].addObject(LudoPawn(LudoColor.BLUE));
        board.tileMap[5][1].addObject(LudoPawn(LudoColor.BLUE));
        board.tileMap[9][1].addObject(LudoPawn(LudoColor.BLUE));
        board.tileMap[10][0].addObject(LudoPawn(LudoColor.BLUE));


        board.tileMap[9][9].addObject(LudoFloor(LudoColor.YELLOW));
        board.tileMap[10][9].addObject(LudoFloor(LudoColor.YELLOW));
        board.tileMap[9][10].addObject(LudoFloor(LudoColor.YELLOW));
        board.tileMap[10][10].addObject(LudoFloor(LudoColor.YELLOW));

        board.tileMap[6][5].addObject(LudoFloor(LudoColor.YELLOW));
        board.tileMap[7][5].addObject(LudoFloor(LudoColor.YELLOW));
        board.tileMap[8][5].addObject(LudoFloor(LudoColor.YELLOW));
        board.tileMap[9][5].addObject(LudoFloor(LudoColor.YELLOW));


        board.tileMap[0][9].addObject(LudoFloor(LudoColor.GREEN));
        board.tileMap[0][10].addObject(LudoFloor(LudoColor.GREEN));
        board.tileMap[1][9].addObject(LudoFloor(LudoColor.GREEN));
        board.tileMap[1][10].addObject(LudoFloor(LudoColor.GREEN));

        board.tileMap[5][6].addObject(LudoFloor(LudoColor.GREEN));
        board.tileMap[5][7].addObject(LudoFloor(LudoColor.GREEN));
        board.tileMap[5][8].addObject(LudoFloor(LudoColor.GREEN));
        board.tileMap[5][9].addObject(LudoFloor(LudoColor.GREEN));

      });
}
