import 'dart:ui';
import 'package:rougelike/com/lucasurbas/cleanboard/objects/objects.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/hex/HexGame.dart';

class SimpleHexagon extends HexGame {
  SimpleHexagon()
      : super(
      boardWidth: 10,
      boardHeight: 10,
      initialCameraSize: 5,
      color: Color(0xff000000),
      initBoard: (board) {
        board.tileMap.forEach((e) {
          e.forEach((tile) {
            tile.addObject(HexFloor());
          });
        });
      });
}
