import 'dart:ui';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/CleanBoardGame.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/objects/objects.dart';
import 'dart:math';

class WoodPlus extends CleanBoardGame {
  WoodPlus()
      : super(
            boardWidth: 36,
            boardHeight: 36,
            initialCameraSize: 17,
            color: Color(0xff343434),
            initBoard: (board) {
              for (var x = 0; x < 36; x++) {
                for (var y = 0; y < 10; y++) {
                  var show = Random().nextInt(100);
                  if (show > 40) {
                    var treeHeight = Random().nextInt(3) + 4;
                    board.tileMap[x][y].addObject(Tree(treeHeight));
                  }
                }
              }
              for (var x = 0; x < 36; x++) {
                board.tileMap[x][11].addObject(Wall());
                board.tileMap[x][15].addObject(Wall());
              }
              for (var x = 0; x < 36; x++) {
                for (var y = 17; y < 36; y++) {
                  var show = Random().nextInt(100);
                  if (show > 40) {
                    var treeHeight = Random().nextInt(3) + 2;
                    board.tileMap[x][y].addObject(Tree(treeHeight));
                  }
                }
              }
            });
}
