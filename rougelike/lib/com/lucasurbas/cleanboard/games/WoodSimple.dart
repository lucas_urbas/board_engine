import 'dart:ui';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/CleanBoardGame.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/objects/objects.dart';

class WoodSimple extends CleanBoardGame {
  WoodSimple()
      : super(
            boardWidth: 120,
            boardHeight: 120,
            initialCameraSize: 10,
            color: Color(0xff000000),
            initBoard: (board) {
              board.tileMap.forEach((e) {
                e.forEach((tile) {
                  tile.addObject(Pole());
                });
              });
            });
}
