import 'dart:collection';
import 'dart:math';
import 'dart:ui';
import 'package:flame/components.dart';
import 'package:flutter/material.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/hex/HexTile.dart';

class HexBoard extends PositionComponent {
  final Paint _paint = Paint();
  final Paint _paintSecond = Paint();
  Rect _boardRect = Rect.fromLTWH(0, 0, 0, 0);

  final int boardWidth;
  final int boardHeight;

  List<List<HexTile>> tileMap;
  Map<int, List<HexTile>> _layers = HashMap<int, List<HexTile>>();

  double _cameraTileSize = 0;
  Point _cameraCanvasPosition;
  Vector2 _cameraCenterPosition;

  HexBoard(this.boardWidth, this.boardHeight) {
    _paint.color = Color.fromARGB(255, 255, 0, 0);
    _paint.style = PaintingStyle.stroke;
    _paint.strokeWidth = 2;

    _paintSecond.color = Color.fromARGB(100, 255, 0, 0);
    _paintSecond.style = PaintingStyle.stroke;
    _paintSecond.strokeWidth = 1.5;

    _initBoard();
  }

  void _initBoard() {
    tileMap = List.generate(boardWidth, (i) => List(boardHeight), growable: false);

    for (var x = 0; x < boardWidth; x++) {
      for (var y = 0; y < boardHeight; y++) {
        tileMap[x][y] = HexTile(this, x, y);
      }
    }
  }

  @override
  void render(Canvas c) {
    if (_cameraCanvasPosition == null || _cameraCenterPosition == null) {
      return;
    }
    c.clipRect(Rect.fromLTWH(0, 0, size.x, size.y));
    c.save();
    c.translate(_cameraCanvasPosition.x, _cameraCanvasPosition.y);

    // double r = _cameraTileSize / 2;
    // Offset center = Offset(r, r);
    // for (var x = 0; x < boardWidth; x++) {
    //   for (var y = 0; y < boardHeight; y++) {
    //     double dx = _cameraTileSize * (x + y * 0.5 - y ~/ 2) * HexMetrics.innerRadius;
    //     double dy = _cameraTileSize * y * HexMetrics.topOffset;
    //     c.drawCircle(center.translate(dx, dy), r, _paintSecond);
    //   }
    // }
    // c.drawRect(_boardRect, _paint);

    for (var i = 0; i < _layers.keys.length; i++) {
      List tilesAtLayer = _layers[i];
      for (HexTile tile in tilesAtLayer) {
        Vector2 tileCenter = Vector2(_cameraCanvasPosition.x + _cameraTileSize * tile.x + _cameraTileSize / 2,
            _cameraCanvasPosition.y + _cameraTileSize * tile.y + _cameraTileSize / 2);

        double layerOffsetX = i * (tileCenter.x - _cameraCenterPosition.x) / 30;
        double layerOffsetY = i * (tileCenter.y - _cameraCenterPosition.y) / 30;

        double dx = _cameraTileSize * (tile.x + tile.y * 0.5 - tile.y ~/ 2) * HexMetrics.innerRadius + layerOffsetX;
        double dy = _cameraTileSize * tile.y * HexMetrics.topOffset + layerOffsetY;
        tile.getPaintLayer(i).render(dx, dy, c);
      }
    }
    c.restore();
  }

  @override
  void update(double t) {}

  void updateCamera(double tileSize, Point cameraCanvasPosition, Vector2 cameraCenterPosition) {
    _cameraCanvasPosition = cameraCanvasPosition;
    _cameraCenterPosition = cameraCenterPosition;

    if (_cameraTileSize != tileSize && tileSize != null) {
      _cameraTileSize = tileSize;

      double width = _cameraTileSize * boardWidth * HexMetrics.innerRadius +
          _cameraTileSize * ((1 - HexMetrics.innerRadius) / 2 + 0.5);
      double height =
          _cameraTileSize * boardHeight * HexMetrics.topOffset + _cameraTileSize * (1 - HexMetrics.topOffset);
      _boardRect = Rect.fromLTWH(0, 0, width, height);

      for (var i = 0; i < _layers.keys.length; i++) {
        List tilesAtLayer = _layers[i];
        double layerAlpha = _layerAlpha(i, _layers.keys.length);
        for (HexTile tile in tilesAtLayer) {
          tile.getPaintLayer(i).updateSize(_cameraTileSize);
          // tile.getPaintLayer(i).updateAlpha(layerAlpha);
        }
      }
    }
  }

  double _layerAlpha(int layer, int topLayer) {
    return (layer * 0.6) / topLayer + 0.4;
  }

  void removeFromHeight(int height, HexTile tile) {
    for (var x = 0; x < height; x++) {
      List tilesAtLayer = _layers[x];
      if (tilesAtLayer != null && tilesAtLayer.contains(tile)) {
        tilesAtLayer.remove(tile);
        if (tilesAtLayer.isEmpty) {
          _layers.remove(x);
        }
      }
    }
  }

  void addToHeight(int height, HexTile tile) {
    for (var x = 0; x < height; x++) {
      List tilesAtLayer = _layers[x];
      if (tilesAtLayer == null) {
        tilesAtLayer = List<HexTile>();
        _layers[x] = tilesAtLayer;
      }
      tilesAtLayer.add(tile);
    }
  }
}

class HexMetrics {
  static const double innerRadius = 0.866025404;
  static const double topOffset = 0.75;
}