import 'dart:ui';
import 'package:flame/components.dart';
import 'dart:math';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/ValueAnimation.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/hex/HexBoard.dart';

class HexCamera extends PositionComponent {
  static const double ANIM_DURATION = 0.3;

  final int _boardWidthTile;
  final int _boardHeightTile;
  double _primarySize;
  double _secondarySize;
  final HexBoard _board;

  final Paint _paint = Paint();
  double _padding = 12.0;

  Vector2 _position = Vector2.zero();
  Vector2 _positionTile = Vector2.zero();
  Point _offset = Point(0, 0);
  double tileSize;

  Rect _cameraRect = Rect.fromLTWH(0, 0, 0, 0);

  double _widthTile;
  double _heightTile;

  ValueAnimation _animPrimarySize = ValueAnimation(0, 0, 0);

  ValueAnimation _animPosX = ValueAnimation(0, 0, 0);
  ValueAnimation _animPosY = ValueAnimation(0, 0, 0);

  TextComponent _tilePositionText = TextComponent(text: "0,0");

  HexCamera(this._boardWidthTile, this._boardHeightTile, this._primarySize, this._board) {
    _paint.color = Color.fromARGB(255, 255, 255, 255);
    _paint.style = PaintingStyle.stroke;
    _paint.strokeWidth = 1;
  }

  @override
  void render(Canvas c) {
    c.drawRect(_cameraRect, _paint);
    c.drawCircle(_cameraRect.center, 2, _paint);

    _tilePositionText.render(c);
  }

  @override
  void update(double t) {
    _animPrimarySize.update(t);
    _animPosX.update(t);
    _animPosY.update(t);

    if (_animPrimarySize.isRunning()) {
      _setSizes(_animPrimarySize.current);
      _updateSizes(false);
    }

    if (_animPosX.isRunning()) {
      double oldY = _position.y;
      _position = Vector2(_animPosX.current, oldY);
    }
    if (_animPosY.isRunning()) {
      double oldX = _position.x;
      _position = Vector2(oldX, _animPosY.current);
    }

    _board.updateCamera(tileSize, _getCanvasPosition(), _getCenterPosition());
  }

  // @override
  // void resize(Size size) {
  //   super.resize(size);
  //   _updateSizes(true);
  // }

  Point _getCanvasPosition() {
    return _offset -
        _position.toPoint() +
        Point(_cameraRect.width / 2, _cameraRect.height / 2) -
        Point(tileSize / 2, tileSize / 2);
  }

  Vector2 _getCenterPosition() {
    return Vector2(_cameraRect.center.dx, _cameraRect.center.dx);
  }

  void zoomIn() {
    double newPrimarySize = (_primarySize - 2).round().toDouble();
    if (newPrimarySize < 3) {
      newPrimarySize = 3;
    }
    _animPrimarySize = ValueAnimation(ANIM_DURATION, _primarySize, newPrimarySize, callback: () {
      _setSizes(newPrimarySize);
      _updateSizes(false);
    })
      ..start();
  }

  void zoomOut() {
    double newPrimarySize = (_primarySize + 2).round().toDouble();

    if (newPrimarySize > 18) {
      newPrimarySize = 18;
    }
    _animPrimarySize = ValueAnimation(ANIM_DURATION, _primarySize, newPrimarySize, callback: () {
      _setSizes(newPrimarySize);
      _updateSizes(false);
    })
      ..start();
  }

  void _setSizes(double newPrimarySize) {
    _primarySize = newPrimarySize;
    if (size.x < size.y) {
      _widthTile = newPrimarySize;
      tileSize = (size.x - (2 * _padding)) / _widthTile;
      _secondarySize = (size.y - (2 * _padding)) / tileSize;
      _heightTile = _secondarySize;
    } else {
      _heightTile = newPrimarySize;
      tileSize = (size.y - (2 * _padding)) / _heightTile;
      _secondarySize = (size.x - (2 * _padding)) / tileSize;
      _widthTile = _secondarySize;
    }
  }

  void _updateSizes(bool recalculate) {
    if (_widthTile == null || _heightTile == null || recalculate) {
      if (size.x < size.y) {
        _widthTile = _primarySize;
        tileSize = (size.x - (2 * _padding)) / _widthTile;
        _secondarySize = (size.y - (2 * _padding)) / tileSize;
        _heightTile = _secondarySize;
      } else {
        _heightTile = _primarySize;
        tileSize = (size.y - (2 * _padding)) / _heightTile;
        _secondarySize = (size.x - (2 * _padding)) / tileSize;
        _widthTile = _secondarySize;
      }
    }

    double width = tileSize * _widthTile;
    double left = (size.x - width) / 2;
    double height = tileSize * _heightTile;
    double top = (size.y - height) / 2;
    _cameraRect = Rect.fromLTWH(left, top, width, height);
    _offset = Point(left, top);

    _tilePositionText.x = _offset.x;
    _tilePositionText.y = _offset.y;

    double xTile = _roundNewXTile(_positionTile.x);
    double yTile = _roundNewYTile(_positionTile.y);

    double newXPos = _calculateNewXPosition(xTile, yTile);
    double newYPos = _calculateNewYPosition(yTile);

    _position = Vector2(newXPos, newYPos);
    _positionTile = Vector2(xTile, yTile);
    _tilePositionText.text = "$xTile,$yTile";
  }

  void addPosition(Vector2 delta) {
    _animPosX.stop();
    _animPosY.stop();
    _position.add(delta);
  }

  void snap() {
    double newYTile =
        ((_position.y + tileSize * HexMetrics.topOffset / 2) ~/ (tileSize * HexMetrics.topOffset)).toDouble();
    double yTile = _roundNewYTile(newYTile);

    double newXTile;
    if (yTile % 2 == 0) {
      newXTile =
          ((_position.x + tileSize * HexMetrics.innerRadius / 2) ~/ (tileSize * HexMetrics.innerRadius)).toDouble();
    } else {
      newXTile =
          ((_position.x) ~/ (tileSize * HexMetrics.innerRadius)).toDouble();
    }

    double xTile = _roundNewXTile(newXTile);

    double newXPos = _calculateNewXPosition(xTile, yTile);
    double newYPos = _calculateNewYPosition(yTile);

    _positionTile = Vector2(xTile, yTile);
    _tilePositionText.text = "$xTile,$yTile";

    _animPosX = ValueAnimation(ANIM_DURATION, _position.x, newXPos, callback: () {
      _position = Vector2(newXPos, newYPos);
    })
      ..start();

    _animPosY = ValueAnimation(ANIM_DURATION, _position.y, newYPos, callback: () {
      _position = Vector2(newXPos, newYPos);
    })
      ..start();
  }

  double _calculateNewXPosition(double tileX, double tileY) {
    return tileSize * (tileX + tileY * 0.5 - tileY ~/ 2) * HexMetrics.innerRadius;
  }

  double _calculateNewYPosition(double tileY) {
    return tileSize * tileY * HexMetrics.topOffset;
  }

  double _roundNewXTile(double tileX) {
    return min(max(tileX, 0), (_boardWidthTile - 1).toDouble());
  }

  double _roundNewYTile(double valueToCheck) {
    return min(max(valueToCheck, 0), (_boardHeightTile - 1).toDouble());
  }
}