import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/Tile.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/objects/shapelayers.dart';

final List palette = [
  Color(0xff4B4B4B),
  Color(0xff525252),
  Color(0xff6E6E6E),
  Color(0xff878787),
  Color(0xffacacac),
  Color(0xffcbcbcb),
  Color(0xffdedede),
  Color(0xffffffff)
];

class Pole extends Stackable {
  Pole() {
    layers.add(RectLayer(Color(0xff4B4B4B), 0.2, 0.2));
    layers.add(RectLayer(Color(0xff6E6E6E), 0.2, 0.2));
    layers.add(RectLayer(Color(0xffA0A0A0), 0.2, 0.2));
    layers.add(RectLayer(Color(0xffE9E9E9), 0.2, 0.2));
    layers.add(RectLayer(Color(0xffffffff), 0.2, 0.2));
  }
}

class LudoFloor extends Stackable {
  LudoFloor(LudoColor color) {
    if (color == null) {
      layers.add(RectLayer(Color(0xffffffff), 0.95, 0.95));
    } else if (color == LudoColor.RED) {
      layers.add(RectLayer(Color(0xffDB8D8D), 0.95, 0.95));
    } else if (color == LudoColor.BLUE) {
      layers.add(RectLayer(Color(0xff84BDE3), 0.95, 0.95));
    } else if (color == LudoColor.YELLOW) {
      layers.add(RectLayer(Color(0xffF7ED84), 0.95, 0.95));
    } else if (color == LudoColor.GREEN) {
      layers.add(RectLayer(Color(0xffB1D7A8), 0.95, 0.95));
    }
  }
}

class Tree extends Stackable {
  Tree(int height) {
    var x = 0;
    for (x = 0; x < height; x++) {
      layers.add(RectLayer(palette[x], 0.2, 0.2));
    }

    layers.add(TreeTopLayer(palette[x]));
  }
}

class Wall extends Stackable {
  Wall() {
    var x = 0;
    for (x = 0; x < 4; x++) {
      layers.add(RectLayer(palette[x], 0.8, 0.1));
    }
  }
}

class ChessFloor extends Stackable {
  ChessFloor(PieceColor pieceColor) {
    var color;
    if (pieceColor == PieceColor.WHITE) {
      color = Color(0xffeeeecf);
    } else {
      color = Color(0xff6d984d);
    }
    layers.add(RectLayer(color, 1, 1));
  }
}

class Pawn extends Stackable {
  Pawn(PieceColor pieceColor) {
    if (pieceColor == PieceColor.WHITE) {
      layers.add(CircleLayer(Color(0xffD6BF97), 0.5));
      layers.add(CircleLayer(Color(0xffDFCAA4), 0.3));
      layers.add(CircleLayer(Color(0xffE5D3B2), 0.3));
      layers.add(CircleLayer(Color(0xffEEDCBC), 0.32));
    } else {
      layers.add(CircleLayer(Color(0xff4C2D1E), 0.5));
      layers.add(CircleLayer(Color(0xff573220), 0.3));
      layers.add(CircleLayer(Color(0xff623823), 0.3));
      layers.add(CircleLayer(Color(0xff724129), 0.35));
    }
  }
}

class Rook extends Stackable {
  Rook(PieceColor pieceColor) {
    if (pieceColor == PieceColor.WHITE) {
      layers.add(CircleLayer(Color(0xffD6BF97), 0.5));
      layers.add(CircleLayer(Color(0xffDFCAA4), 0.3));
      layers.add(CircleLayer(Color(0xffE5D3B2), 0.3));
      layers.add(CircleLayer(Color(0xffEEDCBC), 0.43));
      layers.add(CircleLayer(Color(0xffFBEACC), 0.2));
    } else {
      layers.add(CircleLayer(Color(0xff4C2D1E), 0.5));
      layers.add(CircleLayer(Color(0xff573220), 0.3));
      layers.add(CircleLayer(Color(0xff623823), 0.3));
      layers.add(CircleLayer(Color(0xff724129), 0.43));
      layers.add(CircleLayer(Color(0xff80492D), 0.2));
    }
  }
}

enum PieceColor { WHITE, BLACK }

class LudoPawn extends Stackable {
  LudoPawn(LudoColor color) {
    if (color == LudoColor.RED) {
      layers.add(CircleLayer(Color(0xff78110C), 0.6));
      layers.add(CircleLayer(Color(0xff8E150F), 0.38));
      layers.add(CircleLayer(Color(0xff9D1913), 0.28));
      layers.add(CircleLayer(Color(0xffBA2019), 0.33));
    } else if (color == LudoColor.BLUE) {
      layers.add(CircleLayer(Color(0xff0C3378), 0.6));
      layers.add(CircleLayer(Color(0xff0F488E), 0.38));
      layers.add(CircleLayer(Color(0xff13549D), 0.28));
      layers.add(CircleLayer(Color(0xff1966BA), 0.33));
    }
  }
}

enum LudoColor { BLUE, RED, YELLOW, GREEN }

class HexFloor extends Stackable {
  HexFloor() {
    layers.add(HexagonLayer(Color(0xaaffffff), 0.98));
  }
}