import 'package:flutter/material.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/engine/Tile.dart';
import 'package:rougelike/com/lucasurbas/cleanboard/hex/HexBoard.dart';

class RectLayer implements PaintLayer {
  final Paint _paint = Paint();

  Color _color;
  double _width;
  double _height;

  Rect _boardRect = Rect.fromLTWH(0, 0, 0, 0);

  RectLayer(this._color, this._width, this._height) {
    _paint.color = _color;
  }

  @override
  void render(double dx, double dy, Canvas c) {
    c.drawRect(_boardRect.translate(dx, dy), _paint);
  }

  @override
  void updateSize(double size) {
    var offsetX = (size - (size * _width)) / 2;
    var offsetY = (size - (size * _height)) / 2;
    _boardRect = Rect.fromLTWH(offsetX, offsetY, _width * size, _height * size);
  }

// @override
// void updateAlpha(double alpha) {
//   _paint.color = Color.fromRGBO(_color.red, _color.green, _color.blue, alpha);
// }
}

class CircleLayer implements PaintLayer {
  final Paint _paint = Paint();

  Color _color;
  double _size;

  Offset _circleCenter = Offset(0, 0);

  CircleLayer(this._color, this._size) {
    _paint.color = _color;
  }

  @override
  void render(double dx, double dy, Canvas c) {
    c.drawCircle(_circleCenter.translate(dx, dy), _circleCenter.dx * _size, _paint);
  }

  @override
  void updateSize(double size) {
    _circleCenter = Offset(size / 2, size / 2);
  }

// @override
// void updateAlpha(double alpha) {
//   _paint.color = Color.fromRGBO(_color.red, _color.green, _color.blue, alpha);
// }
}

class HexagonLayer implements PaintLayer {
  final Paint _paint = Paint();

  Color _color;
  double _size;

  Path _hexPath = Path();

  HexagonLayer(this._color, this._size) {
    _paint.color = _color;
    _paint.isAntiAlias = true;
    _paint.style = PaintingStyle.fill;
  }

  @override
  void render(double dx, double dy, Canvas c) {
    c.drawPath(_hexPath.shift(Offset(dx, dy)), _paint);
  }

  @override
  void updateSize(double size) {
    double r = size * _size / 2;
    _hexPath.reset();
    _hexPath.moveTo(size / 2, size / 2 - r);
    _hexPath.lineTo(size / 2 + r * HexMetrics.innerRadius, size / 2 - r / 2);
    _hexPath.lineTo(size / 2 + r * HexMetrics.innerRadius, size / 2 + r / 2);
    _hexPath.lineTo(size / 2, size / 2 + r);
    _hexPath.lineTo(size / 2 - r * HexMetrics.innerRadius, size / 2 + r / 2);
    _hexPath.lineTo(size / 2 - r * HexMetrics.innerRadius, size / 2 - r / 2);
    _hexPath.close();
  }

// @override
// void updateAlpha(double alpha) {
//   _paint.color = Color.fromRGBO(_color.red, _color.green, _color.blue, alpha);
// }
}

class TreeTopLayer implements PaintLayer {
  final Paint _paint = Paint();

  Color _color;

  Path _hexPath = Path();

  TreeTopLayer(this._color) {
    _paint.color = _color;
    _paint.isAntiAlias = true;
    _paint.style = PaintingStyle.fill;
  }

  @override
  void render(double dx, double dy, Canvas c) {
    c.drawPath(_hexPath.shift(Offset(dx, dy)), _paint);
  }

  @override
  void updateSize(double size) {
    double u = size / 10;
    _hexPath.reset();
    _hexPath.moveTo(u * 1, u * 4.5);
    _hexPath.lineTo(u * 3, u * 4.5);
    _hexPath.lineTo(u * 3, u * 3.5);
    _hexPath.lineTo(u * 2, u * 3.5);
    _hexPath.lineTo(u * 2, u * 2.5);
    _hexPath.lineTo(u * 4, u * 2.5);
    _hexPath.lineTo(u * 4, u * 3.5);
    _hexPath.lineTo(u * 6, u * 3.5);
    _hexPath.lineTo(u * 6, u * 2.5);
    _hexPath.lineTo(u * 8, u * 2.5);
    _hexPath.lineTo(u * 8, u * 3.5);
    _hexPath.lineTo(u * 7, u * 3.5);
    _hexPath.lineTo(u * 7, u * 4.5);
    _hexPath.lineTo(u * 9, u * 4.5);
    _hexPath.lineTo(u * 9, u * 5.5);
    _hexPath.lineTo(u * 7, u * 5.5);
    _hexPath.lineTo(u * 7, u * 6.5);
    _hexPath.lineTo(u * 8, u * 6.5);
    _hexPath.lineTo(u * 8, u * 7.5);
    _hexPath.lineTo(u * 6, u * 7.5);
    _hexPath.lineTo(u * 6, u * 6.5);
    _hexPath.lineTo(u * 4, u * 6.5);
    _hexPath.lineTo(u * 4, u * 7.5);
    _hexPath.lineTo(u * 2, u * 7.5);
    _hexPath.lineTo(u * 2, u * 6.5);
    _hexPath.lineTo(u * 3, u * 6.5);
    _hexPath.lineTo(u * 3, u * 5.5);
    _hexPath.lineTo(u * 1, u * 5.5);
    _hexPath.close();
  }

// @override
// void updateAlpha(double alpha) {
//   _paint.color = Color.fromRGBO(_color.red, _color.green, _color.blue, alpha);
// }
}
